﻿Option Explicit On

Public Class PuzzleData

    Public Enum Direction
        DIR_NONE = 0
        DIR_UP = 1
        DIR_LEFT = 2
        DIR_RIGHT = 3
        DIR_DOWN = 4
    End Enum

    Public Enum DirSet
        DIRSET_NONE = 0
        DIRSET_UP = 2
        DIRSET_LEFT = 4
        DIRSET_RIGHT = 8
        DIRSET_DOWN = 16
        DIRSET_ALL = DIRSET_UP Or DIRSET_LEFT Or DIRSET_RIGHT Or DIRSET_DOWN
    End Enum

    Public Structure TWordEntry
        Public xCol As Integer      ' X 座標
        Public yRow As Integer      ' Y 座標
        Public iDir As Direction    ' 弐文字目の方向
        Public nLen As Integer      ' 文字数
        Public sChrs() As Char      ' 文字
    End Structure

    Public Structure TWriteData
        Public xCol As Integer
        Public yRow As Integer
        Public dirNext As Direction
        Public dirSet As DirSet     ' 進める方向のビットセット
        Public nDirCnt As Integer   ' 進める方向の数
    End Structure

    Public Structure TWordAns
        Public nLen As Integer
        Public pData() As TWriteData
    End Structure

    Public m_numCols As Integer
    Public m_numRows As Integer
    Public m_numWords As Integer

    ' 問題データ
    Public m_aryWords() As TWordEntry

    ' 解答データ
    Public m_aryWrite() As TWordAns
    Public m_aryField(,) As Char

    Public Sub copyFrom(ByRef src As PuzzleData)
        Dim wrdLen As Integer

        m_numCols = src.m_numCols
        m_numRows = src.m_numRows
        m_numWords = src.m_numWords

        m_aryWords = src.m_aryWords.Clone()
        m_aryField = src.m_aryField.Clone()

        ReDim m_aryWrite(m_numWords)

        For idxWord = 1 To m_numWords
            wrdLen = m_aryWords(idxWord).nLen
            m_aryWrite(idxWord).nLen = src.m_aryWrite(idxWord).nLen
            m_aryWrite(idxWord).pData = src.m_aryWrite(idxWord).pData.Clone()
        Next
    End Sub

    ''-------------------------------------------------------------------------
    ''    フィールドを初期化する。
    ''-------------------------------------------------------------------------
    Public Function initializeField(ByVal xCols As Integer, ByVal yRows As Integer) As Boolean
        Dim X As Integer, Y As Integer

        ' 配列の要素数を変更する。
        ReDim m_aryField(xCols + 1, yRows + 1)

        ' フィールドを空文字列で初期化する。
        For Y = 1 To yRows
            For X = 1 To xCols
                m_aryField(X, Y) = " "
            Next
        Next

        ' フィールドに番兵を書き込む。
        Y = yRows + 1
        For X = 0 To xCols + 1
            m_aryField(X, 0) = "■"
            m_aryField(X, Y) = "■"
        Next
        X = xCols + 1
        For Y = 0 To yRows + 1
            m_aryField(0, Y) = "■"
            m_aryField(X, Y) = "■"
        Next

        ' メンバ変数にサイズを保存する。
        m_numCols = xCols
        m_numRows = yRows
        Return True
    End Function

    ''-------------------------------------------------------------------------
    ''    ファイルから問題データを読み込む。
    ''-------------------------------------------------------------------------
    Public Function readDataFromFile(ByVal fileName As String) As Boolean
        Dim sr As New System.IO.StreamReader(fileName, System.Text.Encoding.UTF8)
        readDataFromFile = readDataFromStream(sr)
        sr.Close()
    End Function

    ''-------------------------------------------------------------------------
    ''    ストリームから問題データを読み込む。
    ''-------------------------------------------------------------------------
    Public Function readDataFromStream(ByRef sr As System.IO.StreamReader) As Boolean
        Dim strLine As String
        Dim strTemp As String
        Dim xNumCols As Integer, yNumRows As Integer
        Dim X As Integer, Y As Integer
        Dim iDir As Direction
        Dim nLen As Integer
        Dim nCurIdx As Integer = 0
        Dim I As Integer

        Try
            ' 列数と行数を読み込む。
            strLine = sr.ReadLine()
            strTemp = getNextToken(strLine, " ")
            xNumCols = Integer.Parse(strTemp)

            strTemp = getNextToken(strLine, " ")
            yNumRows = Integer.Parse(strTemp)

            ' リストのサイズを読み込む。
            strLine = sr.ReadLine()
            m_numWords = Integer.Parse(strLine)

            ' フィールドデータを初期化する。
            initializeField(xNumCols, yNumRows)
            ReDim m_aryWords(m_numWords)
            ReDim m_aryWrite(m_numWords)

            ' 単語データを読み出す。
            For nCurIdx = 1 To m_numWords
                If (sr.Peek() <= -1) Then
                    MsgBox("データ数が不足しています。")
                    Return False
                End If

                strLine = sr.ReadLine()
                strTemp = getNextToken(strLine, " ")
                X = Integer.Parse(strTemp) + 1

                strTemp = getNextToken(strLine, " ")
                Y = Integer.Parse(strTemp) + 1

                strTemp = getNextToken(strLine, " ")
                Select Case strTemp.ToLower()
                    Case "*" : iDir = Direction.DIR_NONE
                    Case "u" : iDir = Direction.DIR_UP
                    Case "l" : iDir = Direction.DIR_LEFT
                    Case "r" : iDir = Direction.DIR_RIGHT
                    Case "d" : iDir = Direction.DIR_DOWN
                    Case Else
                        MsgBox("無効な方向指示です：" & strTemp)
                        Return False
                End Select

                strTemp = getNextToken(strLine, " ")
                nLen = Len(strTemp)
                With m_aryWords(nCurIdx)
                    .xCol = X
                    .yRow = Y
                    .iDir = iDir
                    ReDim .sChrs(0 To nLen - 1)
                    For I = 0 To nLen - 1
                        .sChrs(I) = Mid(strTemp, I + 1, 1)
                    Next
                    .nLen = nLen
                End With

                With m_aryWrite(nCurIdx)
                    ReDim .pData(0 To nLen - 1)
                    For I = 0 To nLen - 1
                        .pData(I).dirSet = DirSet.DIRSET_ALL
                    Next
                    For I = 0 To nLen - 1
                        m_aryField(X, Y) = m_aryWords(nCurIdx).sChrs(I)
                        With .pData(I)
                            .xCol = X
                            .yRow = Y
                            .dirNext = iDir
                            Select Case iDir
                                Case Direction.DIR_NONE : .dirSet = DirSet.DIRSET_ALL
                                Case Direction.DIR_UP : .dirSet = DirSet.DIRSET_UP : Y = Y - 1
                                Case Direction.DIR_LEFT : .dirSet = DirSet.DIRSET_LEFT : X = X - 1
                                Case Direction.DIR_RIGHT : .dirSet = DirSet.DIRSET_RIGHT : X = X + 1
                                Case Direction.DIR_DOWN : .dirSet = DirSet.DIRSET_DOWN : Y = Y + 1
                            End Select
                        End With
                        .nLen = I + 1
                        If (iDir = Direction.DIR_NONE) Then Exit For
                        iDir = Direction.DIR_NONE
                    Next I
                End With

            Next

        Catch
            Return False
        End Try

        Return True
    End Function

End Class
